import "./../css/styles.scss";
import React from "react";
import ReactDOM from "react-dom";
import Window from "./components/window";

class App extends React.Component {
  constructor(props) { 
    super(props);
    this.state = {
      someoneTyping: [],
      conversation: [
        {'Bonnie':'coucou'},
        {'Clyde': 'ça va?'},
        {'Bonnie': 'oui super, c\'est l\'éclate total'},
      ],
    };
  }
  
  updateConversations(e) {
    this.updateSomeoneTyping(Object.keys(e[e.length-1])[0], true);
    this.setState({
      conversation : this.state.conversation.concat(e),
    })
  }

  updateSomeoneTyping(sender, toRemove = false) {
    if (toRemove){
      this.setState({
        someoneTyping : this.state.someoneTyping.filter(n => n != sender),
      })
    }
    else if (this.state.someoneTyping.indexOf(sender)==-1) {
      const someoneTyping = this.state.someoneTyping;
      someoneTyping.push(sender);
      const someoneTypingWithoutNull = someoneTyping.filter(n => n != null);
      this.setState({
        someoneTyping : someoneTypingWithoutNull,
      })
    }
  }

  render() {
    return (
      <div className="center">
        <div className="col-sm-6">
					<Window 
						sender="Bonnie"
            conversation={this.state.conversation}
            someoneTyping={this.state.someoneTyping}
            updateSomeoneTyping={this.updateSomeoneTyping.bind(this)}
						updateConversations={this.updateConversations.bind(this)} />
				</div>
        <div className="col-sm-6">
        	<Window 
						sender="Clyde" 
						conversation={this.state.conversation}
            someoneTyping={this.state.someoneTyping}
            updateSomeoneTyping={this.updateSomeoneTyping.bind(this)}
						updateConversations={this.updateConversations.bind(this)}/>
				</div>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("app"));
