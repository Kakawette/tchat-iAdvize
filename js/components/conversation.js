import React from "react";

class Conversation extends React.Component {
  constructor(props) {
    super(props);
  }

  getSenderFirstLetter(sender) {
    return sender[0].toUpperCase();
  }

  textAlignment(key) {
    return this.props.sender === key[0] ? "text-right" : "text-left"
  }

  showSomeoneTyping() {
    let otherSender = "";
    if (this.props.someoneTyping.length === 2){
      otherSender = this.props.someoneTyping.filter(n => n != this.props.sender)
    }else if (this.props.someoneTyping.length === 1){
      otherSender = this.props.someoneTyping[0];
    }
    if (!this.props.someoneTyping.length || 
      (this.props.someoneTyping.indexOf(this.props.sender)>-1 && this.props.someoneTyping.length < 2)
    ) return null;
    return (
      <div className="padding-9">
        {otherSender} est en train d'écrire.
      </div>
    )
  }

  render() {
    return (
      <div className="conversation">
        {this.props.conversation.map((line, index) => {
          return (
            <div key={index} className={this.textAlignment(Object.keys(line))}>
              <div className="sender-letter">{this.getSenderFirstLetter(Object.keys(line)[0])}</div>
              <div className="message">{Object.values(line)}</div>
            </div>
          );
        })}
        {this.showSomeoneTyping()}
      </div>
    );
  }
}

export default Conversation;

