import React from "react";
import Conversation from "./conversation";

class Window extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isTyping: false,
      conversation: this.props.conversation,
    };
  }

  sendMessage(e) {
    e.preventDefault(); 
    const conversation = this.state.conversation;
    const newLine = {};
    newLine[this.props.sender] = this.refs.messageToSend.value;
    conversation.push(newLine);
    this.refs.messageToSend.value = "";
    this.props.updateConversations(conversation);
    this.setState({isTyping: !!this.refs.messageToSend.value.length});
  }

  changeStateInput(e) {
    if (!!e.target.value.length ){
      this.props.updateSomeoneTyping(this.props.sender);
    } else {
      this.props.updateSomeoneTyping(this.props.sender, true);
    }
    this.setState({
      isTyping: !!e.target.value.length,
    })
  }

  renderAddingForm() {
    
    const hasMessageWritten = this.state.isTyping ? 'has-message-written' : '';
    const buttonClasses = `btn add-button ${hasMessageWritten}`;

    return (
      <form className="add-form" onSubmit={this.sendMessage.bind(this)}>
        <input 
          onChange={this.changeStateInput.bind(this)} 
          className="add-input" 
          type="text" 
          ref="messageToSend" 
          placeholder="Rédiger un message"/>
        <button className={buttonClasses} type="submit">
          <i>&#8594;</i>
        </button>
      </form>
    );
  }

  render() {
    return (
      <div>
        <div className="conversation-header">{this.props.sender}</div>
        <Conversation
          conversation={this.state.conversation}
          sender={this.props.sender}
          someoneTyping={this.props.someoneTyping}
        />
        {this.renderAddingForm()}
      </div>
    );
  }
}

export default Window;